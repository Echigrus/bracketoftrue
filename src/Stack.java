/**
 * Created by rus-s on 29.01.2017.
 */
class Stack {
    private int size;
    private char[] stackArr;
    private int top;

    public Stack(int max) {
        size = max;
        stackArr = new char[size];
        top = -1;
    }

    public char pop() {
        return stackArr[top--];
    }

    public void push(char j) {
        stackArr[++top] = j;
    }

    public char peek() {
        return stackArr[top];
    }

    public boolean isEmpty() {
        return (top == -1);
    }
}