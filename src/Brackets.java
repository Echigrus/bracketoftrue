/**
 * Created by rus-s on 29.01.2017.
 */

import java.util.Scanner;

public class Brackets {
    private String input;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите скобочное выражение");
        String input = scanner.nextLine();
        Brackets checker = new Brackets(input);
        checker.check();
    }

    public Brackets(String in) {
        input = in;
    }

    public void check() {
        int stackSize = input.length();
        Stack theStack = new Stack(stackSize);

        for (int j = 0; j < input.length(); j++)
        {
            char ch = input.charAt(j);
            switch (ch) {
                case '{':
                case '[':
                case '(':
                    theStack.push(ch);
                    break;

                case '}':
                case ']':
                case ')':
                    if (!theStack.isEmpty()) {
                        char chx = theStack.pop();
                        if ((ch == '}' && chx != '{') || (ch == ']' && chx != '[')
                                || (ch == ')' && chx != '('))
                            System.out.println("Ошибка в символе: " + ch + " с индексом " + j);
                    }

                    else System.out.println("Ошибка в символе: " + ch + " с индексом " + j);
                        break;

                default:break;
            }
        }
        if (!theStack.isEmpty()) System.out.println("В выражении ошибка");
        else System.out.print("fine");
    }
}

